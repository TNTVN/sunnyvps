<?php
	class index
	{
		public $m_index; 
		public function __construct()
		{
			$main_menu = "dashboard";
			$sub_menu = "";
			if(file_exists('model/m_index.php'))
			{
				include 'model/m_index.php';
			}
			$this->m_index= new m_index();
			$totalChanel=$this->m_index->GetTotalChannel();
			$totalUser=$this->m_index->GetTotalUser();
			$totalBlockUser=$this->m_index->GetBlockUser();
			$totalDieChannel=$this->m_index->GetDieChannel();
			$totalConfig=$this->m_index->GetTotalConfig();
			$totalRevenue=$this->m_index->GetTotalRevenue();
			$estimateRevenue=0;
			foreach ($totalRevenue as $Revenue)
            {
                $tmp_revenue= json_decode($Revenue['RevenueReport']);
                $estimateRevenue+= $tmp_revenue[0][1];

            }

			$title = "Quản trị hệ thống";
			if(file_exists('views/_layers/l_head.php'))
			{
				require_once("views/_layers/l_head.php");
			}
			if(file_exists('views/_layers/l_header_menu.php'))
			{
				require_once("views/_layers/l_header_menu.php");
			}
			if(file_exists('views/_layers/l_left_menu.php'))
			{
				require_once("views/_layers/l_left_menu.php");
			}

			if(file_exists('views/v_index.php'))
			{
				include 'views/v_index.php';
			}
			
			if(file_exists('views/_layers/l_footer.php'))
			{
				require_once("views/_layers/l_footer.php");
			}

			if(file_exists('views/_layers/l_script.php'))
			{
				require_once("views/_layers/l_script.php");
			}

		}
	}
	$index = new index();
?>